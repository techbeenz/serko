﻿using System;

namespace SerkoLibrary
{
    public interface IXmlProcesser<T>
    {
        string _XMLString { get; set; }
        T Create(string xmlString);
    }
}
