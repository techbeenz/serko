﻿using System;
using System.Linq;
using System.Collections.Generic;

namespace SerkoLibrary
{
    public interface IService<T>
    { 
        IQueryable<T> List();
        T Add(string data);
        T Read(string ID);
        bool Update(T data);
        bool Delete(string ID);
    }
}

