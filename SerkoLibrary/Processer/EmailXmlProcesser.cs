﻿using System;
using System.Text;
using System.Text.RegularExpressions;
using System.Xml;
using System.Xml.Serialization;

namespace SerkoLibrary
{
    /// <summary>
    /// Email xml processer translate email content to SerkoEventModel
    /// </summary>
    public class EmailXmlProcesser : IXmlProcesser<SerkoEventModel>
    {
        public string _XMLString { get; set; }
 
        /// <summary>
        /// To create SerkoEventModel
        /// </summary>
        /// <param name="emailContent">email content contain XML island</param>
        /// <returns>SerkoEventModel object</returns>
        /// <throw>FormatException</throw>
        public SerkoEventModel Create(string emailContent)
        {
            GenerateXmlString(emailContent);
            if (!ValidateXml(this._XMLString))
            {
                throw new FormatException("XML is not validated");
            }
            var stringReader = new System.IO.StringReader(this._XMLString);
            var serializer = new XmlSerializer(typeof(SerkoEventModel), new Type[] { typeof(SerkoEventModel) });
            var result = serializer.Deserialize(stringReader) as SerkoEventModel;
            result.ID = System.Guid.NewGuid().ToString();
            return result;
        }


        /// <summary>
        /// Generate XML String from String Comtent
        /// It will remove unnessary data, keep only data that whin xml tags
        /// </summary>
        /// <param name="content">string content </param>
        private void GenerateXmlString(string content)
        {
            string pattern = @"<(.*)>(.*)<(/\1)>"; // find all tags and values
            content = content.Replace(System.Environment.NewLine, " ");
            MatchCollection matches = Regex.Matches(content, pattern);
            StringBuilder xmlString = new StringBuilder();
            if (matches.Count > 0)
            {
                xmlString.Append("<Root>");
                foreach (Match m in matches)
                {
                    xmlString.Append("<" + m.Groups[1].Value + ">");
                    xmlString.Append(m.Groups[2].Value);
                    xmlString.Append("<" + m.Groups[3].Value + ">");

                }
                xmlString.Append("</Root>");
            }
            this._XMLString = xmlString.ToString();
        }

        /// <summary>
        /// To Validate xml string
        /// Check if <Total> was contain in the data
        /// </summary>
        /// <param name="xmlData">xml format string</param>
        /// <returns>true/false</returns>
        private bool ValidateXml(string xmlData)
        {
            XmlDocument doc = new XmlDocument();
            try
            {
                doc.LoadXml(xmlData);
                if (doc.SelectSingleNode("//Root/expense/total") == null)
                    return false;
                return true;
            }
            catch{ return false; }
        }
    }
}
