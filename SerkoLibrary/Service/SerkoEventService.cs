﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace SerkoLibrary
{
    /// <summary>
    /// Serko Event CRUD Service
    /// </summary>
    public class SerkoEventService : IService<SerkoEventModel>
    {
        /// <summary>
        /// dataStorage simulate as Database that store SerkoEvent data
        /// </summary>
        private Dictionary<string, SerkoEventModel> dataStorage = new Dictionary<string, SerkoEventModel>();
        private IXmlProcesser<SerkoEventModel> _processer;
        public SerkoEventService(IXmlProcesser<SerkoEventModel> processer) {
            this._processer = processer;
        }

        /// <summary>
        /// ADD Service
        /// </summary>
        /// <param name="data"></param>
        /// <returns>SerkoEventModel</returns>
        public SerkoEventModel Add(string data)
        {
            var item = _processer.Create(data);
            dataStorage.Add(item.ID, item);
            return item;
        }

        /// <summary>
        /// Delete SerkoEvent with ID
        /// </summary>
        /// <param name="ID"></param>
        /// <returns>success/fail</returns>
        public bool Delete(string ID)
        {
            if (dataStorage.ContainsKey(ID))
            {
                return dataStorage.Remove(ID);
            }
            return false;
        }

        /// <summary>
        /// List all SerkoEvent
        /// </summary>
        /// <returns></returns>
        public IQueryable<SerkoEventModel> List()
        {
            return dataStorage.Values.AsQueryable();
        }

        /// <summary>
        /// Read SerkoEvent by ID
        /// </summary>
        /// <param name="ID"></param>
        /// <returns></returns>
        public SerkoEventModel Read(string ID)
        {
            if (dataStorage.ContainsKey(ID))
            {
                return dataStorage[ID];
            }
            throw new KeyNotFoundException("ID not exist");
        }

        /// <summary>
        /// Update SerkoEvent 
        /// </summary>
        /// <param name="data"></param>
        /// <returns></returns>
        public bool Update(SerkoEventModel data)
        {
            if (dataStorage.ContainsKey(data.ID))
            {
                dataStorage[data.ID] = data;
            }
            return true;
        }
    }
}
