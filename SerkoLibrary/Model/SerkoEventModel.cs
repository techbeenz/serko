﻿using System;
using System.Xml.Serialization;


namespace SerkoLibrary
{ 
    [XmlRoot("Root")]
    [XmlInclude(typeof(SerkoEventModel))]
    public class SerkoEventModel
    {
        public string ID { get; set; }
        [XmlElement("expense")]
        public ExpenseModel Expense { get; set; }
        [XmlElement("vendor")]
        public string Vendor { get; set; }
        [XmlElement("description")]
        public string Description { get; set; }
        [XmlElement("date")]
        public string Date { get; set; }
    }


    public class ExpenseModel
    {
        private string _costCenter;
        [XmlElement("cost_centre")]
        public string CostCenter
        {
            get
            {
                return String.IsNullOrEmpty(this._costCenter) ? "UNKNOWN" : _costCenter;
            }
            set { this._costCenter = value; }
        }

        private decimal _total;
        [XmlElement("total")]
        public decimal Total
        {
            get => _total;
            set
            {
                _total = value;
                this.NetTotal = Math.Round(value / (decimal)1.15, 2);
                this.GST = Math.Round(this.Total - this.NetTotal, 2);
            }
        }

        public decimal GST { get; set; }
        public decimal NetTotal { get; set; }

        [XmlElement("payment_method")]
        public string PaymentMethod { get; set; }
    }

}
