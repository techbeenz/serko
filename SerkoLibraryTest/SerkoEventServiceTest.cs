﻿using System;
using System.Linq;
using SerkoLibrary;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace SerkoLibraryTest
{
    [TestClass]
    public class SerkoEventServiceTest
    {
        string emailContent = @"Hi Yvaine,
            Please create an expense claim for the below. Relevant details are marked up as
            requested…
            <expense><cost_centre>DEV002</cost_centre>
            <total>1024.01</total><payment_method>personal card</payment_method>
            </expense>
            From: Ivan Castle
            Sent: Friday, 16 February 2018 10:32 AM
            To: Antoine Lloyd <Antoine.Lloyd@example.com>
            Subject: test
            Hi Antoine,
            Please create a reservation at the <vendor>Viaduct Steakhouse</vendor> our
            <description>development team’s project end celebration dinner</description> on
            <date>Tuesday 27 April 2017</date>. We expect to arrive around
            7.15pm. Approximately 12 people but I’ll confirm exact numbers closer to the day.
            Regards,
            Ivan";
        private SerkoEventService service = new SerkoEventService(new EmailXmlProcesser());



        [TestMethod]
        public void EmailServiceAdd()
        {
            var result = service.Add(emailContent);
            Assert.AreEqual("DEV002", result.Expense.CostCenter);
        }

        [TestMethod]
        public void EmailServiceRead()
        {
            var creteItem = service.Add(emailContent);
            var readItem = service.Read(creteItem.ID);
            Assert.AreEqual(creteItem.ID, readItem.ID);
        }

        [TestMethod]
        public void EmailServiceUpdate()
        {
            var creteItem = service.Add(emailContent);
            creteItem.Description = "Change Description";
            if (service.Update(creteItem))
            {
                var readItem = service.Read(creteItem.ID);
                Assert.AreEqual(creteItem.Description, readItem.Description);
            }
            else { Assert.Fail(); }
        }

        [TestMethod]
        public void EmailServiceDelete()
        {
            var creteItem = service.Add(emailContent);
            Assert.AreEqual(1, service.List().Count());
            if (service.Delete(creteItem.ID))
            {
                Assert.AreEqual(0, service.List().Count());
            }
            else
            {
                Assert.Fail();
            }
        }

    }
}
