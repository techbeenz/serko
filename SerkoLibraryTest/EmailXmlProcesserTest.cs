﻿using System;
using System.Linq;
using SerkoLibrary;
using Microsoft.VisualStudio.TestTools.UnitTesting;


namespace SerkoLibraryTest
{
    [TestClass]
    public class EmailXmlProcesserTest
    {

        [TestMethod]
        public void CreateExpenseFormModel()
        {
            // Arrange
            string emailContent = @"Hi Yvaine,
            Please create an expense claim for the below. Relevant details are marked up as
            requested…
            <expense><cost_centre>DEV002</cost_centre>
            <total>1024.01</total><payment_method>personal card</payment_method>
            </expense>
            From: Ivan Castle
            Sent: Friday, 16 February 2018 10:32 AM
            To: Antoine Lloyd <Antoine.Lloyd@example.com>
            Subject: test
            Hi Antoine,
            Please create a reservation at the <vendor>Viaduct Steakhouse</vendor> our
            <description>development team’s project end celebration dinner</description> on
            <date>Tuesday 27 April 2017</date>. We expect to arrive around
            7.15pm. Approximately 12 people but I’ll confirm exact numbers closer to the day.
            Regards,
            Ivan";

            EmailXmlProcesser processer = new EmailXmlProcesser();
            try
            {
                SerkoEventModel item = processer.Create(emailContent);
                Assert.AreEqual("DEV002", item.Expense.CostCenter);
                Assert.AreEqual((decimal)1024.01, item.Expense.Total);
                Assert.AreEqual((decimal)890.44, item.Expense.NetTotal);
                Assert.AreEqual((decimal)133.57, item.Expense.GST);
                Assert.AreEqual("Viaduct Steakhouse", item.Vendor);
            }
            catch
            {
                Assert.Fail();
            }
        }


        [TestMethod]
        public void CreateExpenseFormModel_UnknowVendor()
        {
            // Arrange
            string emailContent = @"Hi Yvaine,
            Please create an expense claim for the below. Relevant details are marked up as
            requested…
            <expense> with out cost center
            <total>1024.01</total><payment_method>personal card</payment_method>
            </expense>
            From: Ivan Castle
            Sent: Friday, 16 February 2018 10:32 AM
            To: Antoine Lloyd <Antoine.Lloyd@example.com>
            Subject: test
            Hi Antoine,
            Please create a reservation at the <vendor>Viaduct Steakhouse</vendor> our
            <description>development team’s project end celebration dinner</description> on
            <date>Tuesday 27 April 2017</date>. We expect to arrive around
            7.15pm. Approximately 12 people but I’ll confirm exact numbers closer to the day.
            Regards,
            Ivan";

            EmailXmlProcesser processer = new EmailXmlProcesser();
            try
            {
                SerkoEventModel item = processer.Create(emailContent);
                Assert.AreEqual("UNKNOWN", item.Expense.CostCenter);
            }
            catch
            {
                Assert.Fail();
            }
        }



        [TestMethod]
        public void CreateExpenseFormModelShallFailWith_NoTotal()
        {
            string emailContent = @"this message dont have Total in xml isoland.
            <expense><cost_centre>DEV002</cost_centre>
            <payment_method>personal card</payment_method>
            </expense>
            bala bala <vendor>Viaduct Steakhouse</vendor> our
            <description>development team’s project end celebration dinner</description> on
            <date>Tuesday 27 April 2017</date>. We expect to arrive";

            EmailXmlProcesser processer = new EmailXmlProcesser();
            Assert.ThrowsException<FormatException>(() => processer.Create(emailContent));

        }


        [TestMethod]
        public void CreateExpenseFormModelShallFail_NoCloseTag()
        {
            string emailContent = @"this message dont have close tag for expense.
            <expense><cost_centre>DEV002</cost_centre>
            <payment_method>personal card</payment_method>
            bala bala <vendor>Viaduct Steakhouse</vendor> our
            <description>development team’s project end celebration dinner</description> on
            <date>Tuesday 27 April 2017</date>. We expect to arrive";

            EmailXmlProcesser processer = new EmailXmlProcesser();
            Assert.ThrowsException<FormatException>(() => processer.Create(emailContent));

        }

    }
}
