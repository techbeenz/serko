﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using SerkoLibrary;

namespace SerkoRestApplication.Controllers
{
    public class SerkoEventController : ApiController
    {
        private IService<SerkoEventModel> _service;

        public SerkoEventController(SerkoEventService service)
        {
            this._service = service;
        }

        // GET api/SerkoEvent
        public List<SerkoEventModel> Get()
        {
            var list = _service.List().ToList();
            return list;
        }

        // GET api/SerkoEvent/ID
        public IHttpActionResult Get(string id)
        {
            try
            {
                var result = _service.Read(id);
                return Ok(result);
            }
            catch{
                return NotFound();
            }
        }

        // POST api/SerkoEvent
        public IHttpActionResult Post([FromBody]string value)
        {
            try
            {
                return Ok(_service.Add(value));
            }
            catch (Exception ex)
            {
                return Content(HttpStatusCode.BadRequest, ex.Message);
            }
        }

        // PUT api/SerkoEvent/ID
        public void Put(string id, [FromBody]SerkoEventModel value)
        {
            _service.Update(value);
        }

        // DELETE api/SerkoEvent/ID
        public IHttpActionResult Delete(string id)
        {
            bool status = _service.Delete(id);
            if (status)
            {
                return Ok();
            }
            else
            {
                return NotFound();
            }
        }
    }
}
